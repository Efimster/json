pub mod error;

use std::collections::HashMap;
use std::str::Chars;

use error::{JSONError, JSONErrorKind};

#[derive(Debug, PartialEq, Clone)]
pub enum JSON {
    Null,
    Boolean(bool),
    JSONNumber(f64),
    JSONString(String),
    JSONObject(HashMap<String, JSON>),
    JSONArray(Vec<JSON>),
}

impl JSON {
    fn parse_json_object(text: &str) -> Result<JSON, JSONError> {
        let mut members: HashMap<String, JSON> = HashMap::new();
        let mut text = &text[1..text.len() - 1];
        loop {
            let (key, value, rest) = JSON::parse_json_next_key_value_member(text)?;
            members.insert(key.to_string(), value);
            let next_position = match JSON::next_char_position(rest, ',') {
                Some(position) => position + 1,
                None => break,
            };

            text = rest[next_position..].trim();

            if text.len() == 0 {
                break;
            }
        }

        Ok(JSON::JSONObject(members))
    }

    fn parse_json_next_key_value_member(text: &str) -> Result<(String, JSON, &str), JSONError> {
        let text = text.trim_start();
        let mut chars = text.chars();
        if !matches!(chars.next(), Some('"')){
            return Err(JSONErrorKind::Parse.into());
        }
        let key_end = JSON::skip_to_string_end(&mut chars);
        let delimiter_position = chars.position(|ch|ch == ':').ok_or(JSONErrorKind::Parse)?;
        let key = &text[1..key_end];
        let rest: &str = &text[key_end + delimiter_position + 2..];
        let (json, rest) = JSON::parse_json_next_value_member(rest)?;
        Ok((key.to_string(), json, rest))
    }

    fn parse_json_next_value_member(text: &str) -> Result<(JSON, &str), JSONError> {
        let text = text.trim_start();
        let first_char = text.chars().nth(0).ok_or(JSONErrorKind::Parse)?;
        let end_pos: Option<usize> = match first_char {
            '{' => JSON::block_end_position(text, '{', '}'),
            '[' => JSON::block_end_position(text, '[', ']'),
            '"' => JSON::string_end_position(text),
            _ => match JSON::next_char_position(text, ',') {
                Some(value) => Some(value - 1),
                None => Some(text.len() - 1),
            },
        };
        let end_pos = end_pos.ok_or(JSONErrorKind::Parse)?;
        let json = JSON::parse(&text[..=end_pos])?;
        let rest = (&text[end_pos + 1..]).trim_end();

        Ok((json, rest))
    }

    fn parse_json_array(text: &str) -> Result<JSON, JSONError> {
        let mut values: Vec<JSON> = Vec::new();
        let mut text = text[1..text.len() - 1].trim();
        while text.len() > 0 {
            let (value, rest) = JSON::parse_json_next_value_member(text)?;
            values.push(value);

            let next_position = match JSON::next_char_position(rest, ',') {
                Some(position) => position + 1,
                None => break,
            };

            text = rest[next_position..].trim();
        }

        Ok(JSON::JSONArray(values))
    }

    pub fn parse(text: &str) -> Result<JSON, JSONError> {
        let text = text.trim();

        if text == "true" {
            return Ok(JSON::Boolean(true));
        }

        if text == "false" {
            return Ok(JSON::Boolean(false));
        }

        if text == "null" {
            return Ok(JSON::Null);
        }

        let first_char = text.chars().nth(0).unwrap();

        if first_char == '{' {
            return JSON::parse_json_object(text);
        }

        if first_char == '[' {
            return JSON::parse_json_array(text);
        }

        if first_char == '"' {
            let value = text[1..text.len() - 1].to_string();
            return Ok(JSON::JSONString(value));
        }

        let number = text.parse::<f64>();
        match number {
            Ok(number) => Ok(JSON::JSONNumber(number)),
            Err(_) => Err(JSONErrorKind::Parse.into()),
        }
    }

    ///Finds block end position (in bytes, which suitable for slices). Block could be an object {} or array [].
    ///The text should start from "block start" symbol
    fn block_end_position(text: &str, start_block_symbol: char, end_block_symbol: char) -> Option<usize> {
        let mut count = 0;
        let mut chars = text.chars();
        let mut total = 0;
        let mut last_len = 0;
        loop {
            match chars.next() {
                Some(ch) => {
                    total += last_len;
                    last_len = ch.len_utf8();
                    if ch == '"' {
                        total += JSON::skip_to_string_end(&mut chars);
                    } else if ch == start_block_symbol {
                        count += 1;
                    } else if ch == end_block_symbol {
                        count -= 1;
                    }
                    if count == 0 {
                        break;
                    }
                },
                None => break,
            };

        }
        if last_len == 0 {None} else {Some(total)}
    }

    fn string_end_position(text: &str) -> Option<usize> {
        let mut chars = text.chars();
        chars.next();
        let total = JSON::skip_to_string_end(&mut chars);
        Some(total)
    }

    //returns bytes left to string end
    fn skip_to_string_end(chars: &mut Chars) -> usize {
        let mut total = 0;
        let mut last_len = 0;
        let mut escaped = false;
        loop {
            match chars.next() {
                Some(ch) => {
                    total += last_len;
                    last_len = ch.len_utf8();

                    let result = ch == '"' && !escaped;
                    if result {
                        total += last_len;
                        break;
                    }
                    if ch == '\\' {escaped = !escaped} else {escaped = false}
                },
                None => break,
            };
        }
        total
    }

    fn next_char_position(text: &str, to_find: char) -> Option<usize> {
        (&text).chars().position(|ch| {
            ch == to_find
        })
    }

    pub fn stringify(&self) -> String {
        match self {
            JSON::Null => String::from("null"),
            JSON::Boolean(value) => value.to_string(),
            JSON::JSONNumber(value) => value.to_string(),
            JSON::JSONString(value) => format!("\"{}\"", value),
            JSON::JSONObject(members) => {
                let members_string = members
                    .iter()
                    .map(|item| format!("\"{}\":{}", item.0, item.1.stringify()))
                    .collect::<Vec<String>>().join(",");
                format!("{{{}}}", members_string)
            }
            JSON::JSONArray(items) => {
                let items_string = items
                    .iter()
                    .map(|item| item.stringify())
                    .collect::<Vec<String>>().join(",");
                format!("[{}]", items_string)
            }
        }
    }

    pub fn insert<T:Into<JSON>>(mut self, key: &str, value: T) -> JSON {
        debug_assert!(matches!(self, JSON::JSONObject(_)));
        if let JSON::JSONObject(ref mut members) = self {
            members.insert(key.to_string(), value.into());
            self
        } else {
            unreachable!();
        }
    }

    pub fn insert_some<T:Into<JSON>>(mut self, key: &str, value: Option<T>) -> JSON {
        debug_assert!(matches!(self, JSON::JSONObject(_)));
        if let JSON::JSONObject(ref mut members) = self {
            match value {
                None => (),
                Some(value) => {members.insert(key.to_string(), value.into());},
            }
            self
        } else {
            unreachable!();
        }
    }


    #[allow(dead_code)]
    pub fn push(mut self, value: JSON) -> JSON {
        debug_assert!(matches!(self, JSON::JSONArray(_)));
        if let JSON::JSONArray(ref mut values) = self {
            values.push(value);
            self
        } else {
            unreachable!()
        }
    }

    pub fn new_object() -> JSON {
        JSON::JSONObject(HashMap::new())
    }
    #[allow(dead_code)]
    pub fn new_array() -> JSON {
        JSON::JSONArray(Vec::new())
    }

    pub fn resolve_object(&self) -> Result<&HashMap<String, JSON>, JSONError> {
        match self {
            JSON::JSONObject(ref hash) => Ok(hash),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_into_object(self) -> Result<HashMap<String, JSON>, JSONError> {
        match self {
            JSON::JSONObject(hash) => Ok(hash),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn get_property<'a>(props: &'a HashMap<String, JSON>, key: &str) -> Result<&'a JSON, JSONError> {
        props.get(key).ok_or(JSONErrorKind::Resolve.into())
    }

    pub fn remove_property(props: &mut HashMap<String, JSON>, key: &str) -> Result<JSON, JSONError> {
        props.remove(key).ok_or(JSONErrorKind::Resolve.into())
    }

    pub fn resolve_number(&self) -> Result<f64, JSONError> {
        match self {
            JSON::JSONNumber(number) => Ok(*number),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_into_number(self) -> Result<f64, JSONError> {
        match self {
            JSON::JSONNumber(number) => Ok(number),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_string(&self) -> Result<String, JSONError> {
        match self {
            JSON::JSONString(string) => Ok(string.clone()),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_into_string(self) -> Result<String, JSONError> {
        match self {
            JSON::JSONString(string) => Ok(string),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_iter(&self) -> Result<impl Iterator<Item=&JSON>, JSONError> {
        match self {
            JSON::JSONArray(array) => {
                Ok(array.iter().map(|item| item))
            }
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_into_iter(self) -> Result<impl Iterator<Item=JSON>, JSONError> {
        match self {
            JSON::JSONArray(array) => {
                Ok(array.into_iter())
            }
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_boolean(&self) -> Result<bool, JSONError> {
        match self {
            JSON::Boolean(value) => Ok(*value),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }

    pub fn resolve_into_boolean(self) -> Result<bool, JSONError> {
        match self {
            JSON::Boolean(value) => Ok(value),
            _ => Err(JSONErrorKind::Resolve.into())
        }
    }
}

impl<K, V> From<HashMap<K, V>> for JSON
    where
        V:Into<JSON>,
        K:ToString
{
    fn from(data: HashMap<K, V>) -> Self {
        let result = data.into_iter()
            .map(|(key, value)|(key.to_string(), value.into())).collect::<HashMap<String, JSON>>();
        JSON::JSONObject(result)
    }
}

impl<T> From<Vec<T>> for JSON
    where
        T:Into<JSON>,
{
    fn from(val: Vec<T>) -> Self {
        let result = val.into_iter().map(|val|val.into()).collect::<Vec<JSON>>();
        JSON::JSONArray(result)
    }
}

impl<T> From<Option<T>> for JSON
    where T:Into<JSON>,
{
    fn from(val: Option<T>) -> Self {
        match val {
            Some(val) => val.into(),
            None => JSON::Null
        }
    }
}

impl From<usize> for JSON{
    fn from(val: usize) -> Self {
        JSON::JSONNumber(val as f64)
    }
}

impl From<f64> for JSON{
    fn from(val: f64) -> Self {
        JSON::JSONNumber(val)
    }
}

impl From<u8> for JSON{
    fn from(val: u8) -> Self {
        JSON::JSONNumber(val as f64)
    }
}

impl From<String> for JSON{
    fn from(val: String) -> Self {
        JSON::JSONString(val)
    }
}

impl From<&str> for JSON{
    fn from(val: &str) -> Self {
        JSON::JSONString(val.to_string())
    }
}

impl From<bool> for JSON{
    fn from(val: bool) -> Self {
        JSON::Boolean(val)
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_boolean() {
        match JSON::parse("true") {
            Ok(JSON::Boolean(value)) => assert_eq!(value, true),
            _ => panic!("not a JSON::Boolean")
        }

        match JSON::parse("false") {
            Ok(JSON::Boolean(value)) => assert_eq!(value, false),
            _ => panic!("not a JSON::Boolean")
        }
    }

    #[test]
    fn test_parse_null() {
        match JSON::parse("null") {
            Ok(JSON::Null) => (),
            _ => panic!("not a JSON::null")
        }
    }

    #[test]
    fn test_parse_string() {
        let sentence = "\"Hello\t there! \u{AA} \"";

        match JSON::parse(sentence) {
            Ok(JSON::JSONString(value)) => assert_eq!(value, "Hello\t there! \u{AA} "),
            _ => panic!("not a JSON::JSONString")
        };
    }

    #[test]
    fn test_parse_number() {
        match JSON::parse("1.2E10") {
            Ok(JSON::JSONNumber(value)) => assert_eq!(value, 1.2e10),
            _ => panic!("not a JSON::JSONNumber")
        };
    }

    #[test]
    fn test_parse_object() {
        let object = "{\"messageId\":1,\"type\":\"get-offers\",\"originator\":\"origname\"}";
        match JSON::parse(object) {
            Ok(JSON::JSONObject(members)) => {
                assert_eq!(members.len(), 3);

                match members.get(&String::from("messageId")) {
                    None => panic!("no messageId key in JSON object"),
                    Some(json) => match json {
                        JSON::JSONNumber(value) => assert_eq!(*value, 1f64),
                        _ => panic!("wrong messageId value")
                    }
                };
            }
            _ => panic!("not a JSON::JSONObject")
        };
    }

    #[test]
    fn test_parse_complex_object() -> Result<(), JSONError>{
        let object = "{\"key1\":{\"key2\":\"val\\\"ue}{\",\"key3\":{\"key4\":\"val\\\"ue}{\"},\"key5\":3}}";
        let json = JSON::parse(object)?;
        let compare = JSON::new_object()
            .insert("key1", JSON::new_object()
                .insert("key2", JSON::JSONString("val\\\"ue}{".to_string()))
                .insert("key5", JSON::JSONNumber(3.0))
                .insert("key3", JSON::new_object()
                    .insert("key4", JSON::JSONString("val\\\"ue}{".to_string()))
                )
            );
        assert_eq!(json, compare);
        Ok(())
    }

    #[test]
    fn test_parse_complex_object_with_array() -> Result<(), JSONError> {
        let object = "[1,{\"key1\":{\"key2\":[\"val\\\"ue}{][\",{\"key3\":\"val\\\"ue[]][}{\"},2]}},3]";
        let json = JSON::parse(object)?;
        let compare = JSON::new_array()
            .push(JSON::JSONNumber(1.0))
            .push(JSON::new_object()
                .insert("key1", JSON::new_object()
                    .insert("key2", JSON::new_array()
                        .push(JSON::JSONString("val\\\"ue}{][".to_string()))
                        .push(JSON::new_object()
                            .insert("key3", JSON::JSONString("val\\\"ue[]][}{".to_string()))
                        )
                        .push(JSON::JSONNumber(2.0))
                    )
                )
            )
            .push(JSON::JSONNumber(3.0));

        assert_eq!(json, compare);
        Ok(())
    }

    #[test]
    fn test_parse_array() {
        let array = "[1,2,3,true]";
        match JSON::parse(array) {
            Ok(JSON::JSONArray(items)) => {
                assert_eq!(items.len(), 4);
                match items[3] {
                    JSON::Boolean(value) => assert!(value),
                    _ => panic!("unexpected json value in the array")
                }
            }
            _ => panic!("not a JSON::JSONArray")
        }
    }

    #[test]
    fn test_stringify_boolean() {
        assert_eq!(JSON::Boolean(true).stringify(), "true");
        assert_eq!(JSON::Boolean(false).stringify(), "false");
    }

    #[test]
    fn test_stringify_null() {
        assert_eq!(JSON::Null.stringify(), "null");
    }

    #[test]
    fn test_stringify_string() {
        assert_eq!(JSON::JSONString(String::from("Hello")).stringify(), "\"Hello\"");
    }

    #[test]
    fn test_stringify_number() {
        assert_eq!(JSON::JSONNumber(1.2).stringify(), "1.2")
    }

    #[test]
    fn test_stringify_object() {
        let mut members: HashMap<String, JSON> = HashMap::new();
        members.insert(String::from("messageId"), JSON::JSONNumber(1f64));
        members.insert(String::from("type"), JSON::JSONString(String::from("get-offers")));
        members.insert(String::from("originator"), JSON::JSONString(String::from("origname")));

        let result = JSON::JSONObject(members).stringify();

        assert!(result.contains("\"messageId\":1"));
        assert!(result.contains("\"type\":\"get-offers\""));
        assert!(result.contains("\"originator\":\"origname\""));
    }

    #[test]
    fn test_stringify_array() {
        assert_eq!(JSON::JSONArray(vec![]).stringify(), "[]");
        let items: Vec<JSON> = vec![JSON::JSONNumber(1f64), JSON::JSONString(String::from("2")),
                                    JSON::Boolean(false), JSON::Null];
        assert_eq!(JSON::JSONArray(items).stringify(), "[1,\"2\",false,null]");
    }

    #[test]
    fn test_parse_object2() -> Result<(), JSONError> {
        let text = "{\"y\":\"w’z\"}";
        let _ = JSON::parse_json_object(text)?;
        Ok(())
    }

    #[test]
    fn test_parse_object3() -> Result<(), JSONError> {
        let text = "{\"e\": \"x\\\\\",\"val\": 1}";
        let json = JSON::parse_json_object(text)?;
        let e = json.resolve_object()?.get("e").unwrap().resolve_string()?;
        assert_eq!(e, "x\\\\");
        Ok(())
    }

    #[test]
    fn test_parse_object4() -> Result<(), JSONError> {
        let text = "{\"https://test.com\": \"x\"}";
        let json = JSON::parse_json_object(text)?;
        let e = json.resolve_object()?.get("https://test.com").unwrap().resolve_string()?;
        assert_eq!(e, "x");
        Ok(())
    }

    #[test]
    fn test_hash_map_to_json() -> Result<(), JSONError> {
        let mut members: HashMap<String, usize> = HashMap::new();
        members.insert(String::from("1"), 1);
        members.insert(String::from("2"), 2);
        members.insert(String::from("3"), 3);

        let result:JSON = members.into();
        assert!(matches!(result, JSON::JSONObject(_)));
        let result = result.resolve_object()?;
        let result = result.get("1").unwrap();
        assert_eq!(result, &JSON::JSONNumber(1f64));
        Ok(())
    }

    #[test]
    fn test_parse_empty_array() -> Result<(), JSONError> {
        let json = JSON::parse("[]")?;
        let e = json.resolve_iter()?.collect::<Box<_>>();
        assert_eq!(e.len(), 0);
        Ok(())
    }
}
