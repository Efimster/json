use std::error::Error;
use std::fmt;


#[derive(Debug)]
pub struct JSONError {
    pub source:Option<Box<dyn Error>>,
    pub kind:JSONErrorKind,
}

#[derive(Debug, Clone, Copy)]
pub enum JSONErrorKind {
    Parse,
    Resolve,
    Inner
}

impl Error for JSONError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl JSONErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:JSONError = self.into();
        error.into()
    }
}


impl fmt::Display for JSONErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            JSONErrorKind::Parse => write!(f, "JSON parse error"),
            JSONErrorKind::Resolve => write!(f, "error while parsing a string to JSON"), 
            JSONErrorKind::Inner =>  write!(f, "error in JSONError::source"),
        } 
    }
}

impl fmt::Display for JSONError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("{}", self.kind))
    }
}

impl JSONError {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        JSONError {
            source: Some(source),
            kind: JSONErrorKind::Inner
        }
    }

    pub fn new_kind(kind:JSONErrorKind)->Self{
        JSONError {
            source: None,
            kind
        }
    }
}

impl From<JSONErrorKind> for JSONError {
    fn from(kind: JSONErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for JSONError {
    fn from(err: Box<dyn Error>) -> Self {
        Self::new_inner(err)
    }
}
